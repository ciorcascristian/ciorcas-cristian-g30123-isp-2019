package ciorcas.cristian.lab3.ex4;

public class TestMyPoint {
    public static void main(String[] args){
        MyPoint A = new MyPoint();
        MyPoint B = new MyPoint(2,2);
        System.out.println("A" + A.toString());
        A.setX(3);
        A.setY(5);
        System.out.println("A" + A.toString());
        A.distance(10,9);
        A.setXY(5,6);
        System.out.println("A" + A.toString());
        System.out.println("Bx is "+B.getX());
        System.out.println("By is "+B.getY());
        A.distance(B);
    }
}
