package ex7;

import java.util.Scanner;

public class GuessGame {
    public static void main(String[] args) {
        int r;
        Scanner num = new Scanner(System.in);
        int number;
        int tries = 0;
        boolean repeat;
        char key;

        do {
            r = (int) (Math.random() * 100);
            System.out.println("To start the game, choose a number between 0 and 100. ");
            while (tries < 3) {
                number = num.nextInt();
                if (number < r)
                    System.out.println("Wrong answer, your number is too low. ");
                if (number == r) {
                    System.out.println("Congratulation,YOU WON. ");
                    break;
                }
                if (number > r)
                    System.out.println("Wrong answer, your number it too high. ");
                tries++;
            }
            if (tries >= 2)
                System.out.print("The correct number was: " + r + ", better luck next time. ");
            System.out.println("Do you want to try again?Press Y for yes or N for no");
            key = num.next().charAt(0);
            if (key == 'y' || key == 'Y')
                repeat = true;
            else if (key == 'n' || key == 'N')
                repeat = false;
            else {
                System.out.println("Wrong key! Game over!");
                repeat = false;
            }
            tries = 0;
        } while (repeat);
    }
}

