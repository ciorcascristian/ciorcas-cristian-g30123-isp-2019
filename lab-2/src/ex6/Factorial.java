package ex6;

import java.util.Scanner;

public class Factorial {
    static int recursive(int N,int factorial) {

        if(N>=1)
        {factorial=factorial*N;
         recursive(N-1,factorial);
        }
        System.out.println("N!=" + factorial);
        return 0;

    }
    public static void main(String[] args) {

        Scanner num = new Scanner(System.in);
        System.out.println("N=");
        int N = num.nextInt();
        System.out.println("Choose the method for computing N! ");
        System.out.println("1) Non recursive method");
        System.out.println("2) Recursive method");
        int o = num.nextInt();
        int factorial = 1;
        switch (o) {
            case 1: {
                for (int i = 1; i <= N; i++)
                    factorial = factorial * i;
                System.out.println("N!=" + factorial);
                break;
            }
            case 2: {recursive(N,factorial);
                break;
            }
            default: {
                System.out.println("Error,choose different method");
                break;
            }
        }
    }
}