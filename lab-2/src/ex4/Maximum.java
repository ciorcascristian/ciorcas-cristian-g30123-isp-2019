package ex4;

import java.util.Scanner;

public class Maximum {

    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        int[] anArray;
        System.out.println ("Enter the number of elements ");

        int N = num.nextInt();
        anArray = new int[N];

        for (int i = 0; i < N; i++) {
            anArray[i] = num.nextInt();
        }
        int max = anArray[0];
        for (int i = 0; i < N; i++)
            if (max <= anArray[i])
                max = anArray[i];

        System.out.println("The maximum element of the vector is " + max);
    }
}
