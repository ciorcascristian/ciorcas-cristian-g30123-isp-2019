package ex2;
import java.util.Scanner;

    public class PrintNumber {

        public static void main(String[] args){
            Scanner a = new Scanner(System.in);
            System.out.println("The number you want(1 to 9): " + "");
            int x = 1;
            while(x != 0) {
                x = a.nextInt();
                String number;
                switch (x){
                    case 1:
                        number = "ONE";
                        break;
                    case 2:
                        number = "TWO";
                        break;
                    case 3:
                        number = "THREE";
                        break;
                    case 4:
                        number = "FOUR";
                        break;
                    case 5:
                        number = "FIVE";
                        break;
                    case 6:
                        number = "SIX";
                        break;
                    case 7:
                        number = "SEVEN";
                        break;
                    case 8:
                        number = "EIGHT";
                        break;
                    case 9:
                        number = "NINE";
                        break;
                    default:
                        number = "OTHER";
                        break;
                }

                System.out.println(number);

            }a.close();
        }

    }