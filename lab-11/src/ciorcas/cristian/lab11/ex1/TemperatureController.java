package ciorcas.cristian.lab11.ex1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TemperatureController  {
    Sensor t;
    Display tview;
    public TemperatureController(Sensor t, Display tview){
        t.addObserver(tview);
        this.t = t;
        this.tview = tview;
    }
}