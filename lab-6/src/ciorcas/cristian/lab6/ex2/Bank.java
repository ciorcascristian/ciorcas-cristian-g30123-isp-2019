package ciorcas.cristian.lab6.ex2;
import ciorcas.cristian.lab6.ex1.BankAccount;

import java.util.ArrayList;
import java.util.Comparator;

public class Bank {
    private ArrayList<BankAccount> accounts = new ArrayList<>();

    public void addAccount(String owner, double balance) {
        accounts.add(new BankAccount(owner, balance));
    }

    public void printAccounts() {
        accounts.sort(Comparator.comparingDouble(BankAccount::getBalance));

        for (BankAccount i : accounts)
            System.out.println("Owner: "+ i.getOwner() + ", Balance: " + i.getBalance());
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount i : accounts)
            if (i.getBalance() >= minBalance && i.getBalance() <= maxBalance)
                System.out.println("Owner: "+ i.getOwner() + ", Balance: " + i.getBalance());
    }

    @Override
    public String toString() {
        return "Bank{" + "accounts=" + accounts + '}' +"\n";
    }

    public String getAllAccounts() {
        accounts.sort(Comparator.comparing(BankAccount::getOwner));
        return "\n" + accounts;
    }
}
