package ciorcas.cristian.lab5.ex1;

public class Circle extends Shape {
    protected double radius;
    public Circle(){
        radius=1.00;
    }
    public Circle(double radius){
        this.radius=radius;
    }
    public Circle(double radius, String color,boolean filled){
        this.radius=radius;
        this.color=color;
        this.filled=filled;
    }
    public double getRadius(){
        return radius;
    }
    public void setRadius(double radius){
        this.radius=radius;
    }
    public  double getArea(){
        return this.radius*this.radius*Math.PI;
    }
    public  double getPerimeter(){
        return 2*Math.PI*this.radius;
    }
    public String toString(){
        return "Circle with radius " + radius + " is a subclass of " + getClass().getSuperclass().getSimpleName();
    }
}
