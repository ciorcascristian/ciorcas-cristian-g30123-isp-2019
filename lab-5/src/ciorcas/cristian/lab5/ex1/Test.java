package ciorcas.cristian.lab5.ex1;

public class Test {
    public static void main(String[] args) {
        Shape [] arr=new Shape[3];
        arr[0]=new Circle(1);
        arr[1]=new Rectangle(2,5);
        arr[2]=new Square(2);
        System.out.println("Cirlce area: " + arr[0].getArea() + "\n"+ "Circle perimeter: " + arr[0].getPerimeter());
        System.out.println("Rectangle area: " + arr[1].getArea() + "\n"+ "Rectangle perimeter: " + arr[1].getPerimeter());
        System.out.println("Square area: " + arr[2].getArea() + "\n"+ "Square perimeter: " + arr[2].getPerimeter());

    }
}
