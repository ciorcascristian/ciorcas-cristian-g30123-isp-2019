package ciorcas.cristian.lab9.ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Counter extends JFrame {

    static float presses = 0;
    static int number = 0;
    static JLabel counter;
    static JTextArea count;
    JButton doSmt;
    static int width = 80;
    static int height = 20;

    private static boolean running = true;

    Counter() {
        setTitle("Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(370, 310);
        setVisible(true);
    }

    private void init() {
        this.setLayout(null);

        counter = new JLabel("Counter:");
        counter.setBounds(100, 90, width, height);

        count = new JTextArea();
        count.setBounds(160, 90, width, height);
        count.setEnabled(false);
        count.setText(String.valueOf(number));

        doSmt = new JButton("Press");
        doSmt.setBounds(100, 120, 145, 50);
        doSmt.addActionListener(new doSmtOnPress());


        add(counter);
        add(count);
        add(doSmt);
    }

    public static void main(String[] args) {
        new Counter();
        run();
    }

    public static void run() {
        float clicks = 0;
        int countTick = 0;

        while (running) {
            clicks = presses;
        }
    }
}

class doSmtOnPress implements ActionListener {


    @Override
    public void actionPerformed(ActionEvent e) {
        Counter.presses++;
        Counter.number++;
        Counter.count.setText(null);
        Counter.count.setText(String.valueOf(Counter.number));
    }
}