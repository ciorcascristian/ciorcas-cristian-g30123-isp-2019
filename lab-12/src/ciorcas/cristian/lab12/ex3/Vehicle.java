package ciorcas.cristian.lab12.ex3;

public class Vehicle {

    private String type;
    private int weight;

    public Vehicle(String type, int length) {
        this.type = type;
        this.weight=length;
    }

    public boolean equals(Vehicle v){
        if(v==this)
            return true;
        if(!(v instanceof Vehicle))
            return false;
        return type==v.type && weight==v.weight;
    }

    public int getWeight(){
        return this.weight;
    }


    public String start(){
        return "engine started";
    }

}

