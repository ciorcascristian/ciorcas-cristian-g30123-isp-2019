package ciorcas.cristian.lab12.ex3;

public class BatteryException extends Exception {

    BatteryException(String msg){
        super(msg);
    }

}