package ciorcas.cristian.lab12.ex3;

import java.util.ArrayList;
import java.util.Comparator;

public class Parking {

    ArrayList<Vehicle> parkedVehicle=new ArrayList<Vehicle>();

    public void parkVehicle(Vehicle e){
        parkedVehicle.add(e);
    }

    /**
     * Sort vehicles by length.
     */
    public void sortByWeight(){
        parkedVehicle.sort(Comparator.comparing(Vehicle::getWeight));
    }

    public Vehicle get(int index){
        for(int i=0;i<parkedVehicle.size();i++)
            if(i==index)
                return parkedVehicle.get(i);
        return null;
    }

}

