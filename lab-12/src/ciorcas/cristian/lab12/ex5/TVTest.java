package ciorcas.cristian.lab12.ex5;
import org.junit.Test;
import static org.junit.Assert.*;

public class TVTest {
    @Test
    public void channels(){
        TV lg = new TV();
        TV samsung=new TV();
        lg.channelUp();lg.channelUp();lg.channelUp();lg.channelDown();
        samsung.channelUp();samsung.channelUp();samsung.channelDown();
        assertEquals(2,lg.getChannel());
        assertEquals(1,samsung.getChannel());
    }
}
