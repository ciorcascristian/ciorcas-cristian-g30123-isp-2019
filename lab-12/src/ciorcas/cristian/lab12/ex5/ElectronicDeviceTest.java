package ciorcas.cristian.lab12.ex5;
import org.junit.Test;
import static org.junit.Assert.*;

public class ElectronicDeviceTest {
    @Test
    public void power() {
        ElectronicDevice electro1=new ElectronicDevice();
        ElectronicDevice electro2=new ElectronicDevice();
        electro1.turnOn();
        electro2.turnOff();
        assertTrue(electro1.isPowered());
        assertFalse(electro2.isPowered());
    }
}
