package ciorcas.cristian.lab4.ex6;

public class TestClass {
    public static void main(String[] args) {
        Shape s = new Shape("red", false);
        s.setColor("orange");
        s.setFill(true);
        System.out.println(s.toString()); //getColor,getFill

        Circle c = new Circle("blue", 5, true);
        c.setRadius(3);
        System.out.println("Area of the circle is " + c.getArea());
        System.out.println("Perimeter of the circle is " + c.getPerimeter());
        System.out.println("Radius of the circle is " + c.getRadius());
        System.out.println(c.toString());
        Rectangle r = new Rectangle("red", true, 1, 2);
        r.setLenght(3);
        r.setWidth(2);
        System.out.println("Rectangle has a area of "+ r.getArea() + " a perimeter of " + r.getPerimeter() + " and has the lenght,width of "
        + r.getLenght() + " ," + r.getWidth());
        System.out.println(r.toString());

        Square p = new Square("black", true, 4);
        p.setLenght(1);
        p.setWidth(2);
        p.setSide(3);
        System.out.println("Square with");
        System.out.println("Lenght:"+p.getLenght());
        System.out.println("Width:"+p.getWidth());
        System.out.println("Side:"+p.getSide());
        System.out.println(p.toString());
    }
}
