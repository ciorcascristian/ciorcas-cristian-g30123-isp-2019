package ciorcas.cristian.lab4.ex6;

public class Square extends Rectangle {
    public Square()
    {
        super();
    }

    public Square(double side) {

        super(side, side);
    }

    public Square(String color, boolean filled, double side)
    {
        super(color, filled, side, side);
    }

    public double getSide() {
        return super.getWidth();
    }

    public void setSide(double side) {
        super.setWidth(side);
        super.setLenght(side);
    }

    public void setWidth(double side) {
        super.setWidth(side);
        super.setLenght(side);
    }

    public void setLenght(double side) {
        super.setWidth(side);
        super.setLenght(side);
    }

    public String toString() {
        return "Square with side " + this.getSide() + " which is a subclass of " + getClass().getSuperclass().getSimpleName();
    }
}
