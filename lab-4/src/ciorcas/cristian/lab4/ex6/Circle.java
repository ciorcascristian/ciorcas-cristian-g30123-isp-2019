package ciorcas.cristian.lab4.ex6;

public class Circle extends Shape {
    private  double radius;
    public Circle(){
        radius=1.0;
    }
    public Circle(double radius){

        this.radius=radius;
    }
    public Circle(String color,double radius,boolean fill){
        this.color=color;
        this.radius=radius;
        this.fill=fill;
    }

    public void setRadius(double radius) {

        this.radius = radius;
    }

    public double getRadius(){

        return this.radius;
    }
    public double getArea(){

        return this.radius*this.radius*Math.PI;
    }

    public double getPerimeter() {

        return 2 * Math.PI * this.radius;
    }
    public String toString() {
        return "Circle with radius " + radius + " is a subclass of " + getClass().getSuperclass().getSimpleName();
    }
}
