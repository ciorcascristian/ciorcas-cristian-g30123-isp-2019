package ciorcas.cristian.lab4.ex6;

public class Shape {
    public String color;
    public boolean fill;

    public Shape() {
        color = "red";
        fill = true;
    }

    public Shape(String color, boolean fill) {
        this();
        this.color = color;
        this.fill = fill;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean getFill() {
        return fill;
    }

    public void setFill(boolean fill) {
        this.fill = fill;
    }

    public String toString() {
        return "color=" + getColor() + ", filled=" + getFill() ;
    }
}
