package ciorcas.cristian.lab4.ex4;

import ciorcas.cristian.lab4.ex2.Author;
import java.util.Arrays;

public class TestBook {
    public static void main(String[] args) {
        Author[] authors = new Author[2];
        authors[0] = new Author("Ionescu", "ionescu@gmail.com", 'M');
        authors[1] = new Author("Popescu", "popescu@gmail.com", 'M');
        Book book1 = new Book("Abcedar",authors,15,10);
        String name = book1.getName();
        String author = Arrays.toString(book1.getAuthors());
        double price = book1.getPrice();
        int qty = book1.getQtyInStock();
        System.out.println("Book name: " + name);
        System.out.println("Author: " + author);
        System.out.println("Price: " + price);
        System.out.println("Quantity: " + qty);
        book1.setPrice(19.99);
        book1.setQtyInStock(30);
        System.out.println("Price: " + book1.getPrice());
        System.out.println("Quantity: " + book1.getQtyInStock());
        System.out.println(book1.toString());
    }
}







