package ciorcas.cristian.lab4.ex4;

import ciorcas.cristian.lab4.ex2.Author;
import java.util.Arrays;

public class Book {
    private String name;
    private double price;
    private int qtyInStock=0;
    private Author[] authors;

    public Book(String name, Author [] authors, double price){
        this.name=name;
        this.authors=authors;
        this.price=price;
    }
    public Book(String name,Author [] authors,double price,int qtyInStock){
        this.name=name;
        this.authors=authors;
        this.price=price;
        this.qtyInStock=qtyInStock;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public String getName() {
        return this.name;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {

        return this.qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {

        this.qtyInStock = qtyInStock;
    }

    public String toString() {
        return name + " by " + authors.length + " authors";
    }
    public void printAuthors() {
        System.out.println(Arrays.toString(authors));
    }
}
