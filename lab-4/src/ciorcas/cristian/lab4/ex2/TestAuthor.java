package ciorcas.cristian.lab4.ex2;

public class TestAuthor {
    public static void main(String[] args){
        Author autor1 = new Author("Popescu Ion","popescu_ion@email.com",'M');
        autor1.getName();
        autor1.getGender();
        autor1.getEmail();
        autor1.setEmail("ion_popescu@email.com");
        autor1.getEmail();
        System.out.println(autor1.toString());
    }
}
