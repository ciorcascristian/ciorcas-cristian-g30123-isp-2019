package ciorcas.cristian.lab4.ex3;
import ciorcas.cristian.lab4.ex2.Author;
public class TestBook {
    public static void main(String[] args){
        Author autor1 = new Author("Popescu Ion","popescu_ion@email.com",'M');
        Book book1=new Book("Moarte pe Nill",autor1,34.99,10);
        String name= book1.getName();
        String author= String.valueOf(book1.getAuthor());
        double price=book1.getPrice();
        int qty=book1.getQtyInStock();
        System.out.println("Name: "+name);
        System.out.println("Author: "+author);
        System.out.println("Price: "+price);
        System.out.println("Quantity: "+qty);
        book1.setPrice(19.99);
        book1.setQtyInStock(30);
        System.out.println("Price: "+book1.getPrice());
        System.out.println("Quantity: "+book1.getQtyInStock());
    }
}
