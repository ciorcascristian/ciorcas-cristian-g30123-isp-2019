package ciorcas.cristian.lab7.ex2;
import java.io.File;
import java.util.Scanner;
public class Test {
    public static void main(String[] args){
        Scanner scanner=new Scanner(System.in);
        CharCounter c=new CharCounter();
        File fileName=new File("data.txt");
        char ch;
        System.out.println("Enter a character: ");
        ch=scanner.next().charAt(0);
        c.charFinder(fileName,ch);
    }
}
