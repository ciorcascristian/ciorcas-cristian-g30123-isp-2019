package ciorcas.cristian.lab7.ex2;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
public class CharCounter {
    private int charCount;

    public void charFinder(File fileName, char ch) {
        try (Scanner input = new Scanner(fileName)) {
            while (input.hasNextLine()) {
                String line = input.nextLine();
                for (int i = 0; i < line.length(); i++) {
                    if (line.toLowerCase().charAt(i) == ch) {
                        charCount++;
                    }
                }
            }
            System.out.println("Total number of '"+ch+"' is "+charCount);
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
