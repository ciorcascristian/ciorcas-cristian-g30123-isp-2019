package ciorcas.cristian.lab7.ex1;

public class TooManyObjectsException extends Exception {

    public TooManyObjectsException(String msg) {
        super(msg);
    }

}
